const { Pool } = require('pg')
const moment = require('moment');

// Verificación de datos de DB y creación de POOL:
const connectionString = process.env.DATABASE_URL
if (!connectionString) {
  console.error('No se ha definido DATABASE_URL!')
  process.exit(1)
}

const pool = new Pool({ connectionString })

const saveUsers = async (name, surname, email, password) => {
  const client = await pool.connect();
  let query = `SELECT email FROM runrun.organizers WHERE email = '${email}' `;
  let result = await pool.query(query);
  if (result.rows.length !== 0) {
    throw Error('Usuario existente!');
  }
  console.log(name, surname, email, password);
  query = `INSERT INTO runrun.organizers (organizer_name, surname, email, hash) 
  values ('${name}', '${surname}','${email}','${password}')`;
  result = await pool.query(query);
  if (result.rowCount === 0) {
    throw Error('Error de inserción!');
  }
  client.release();
  return;
}

const updateUsers = async (name, surname, email, password, id_organizer) => {
  const client = await pool.connect();

  if (id_organizer) {
    await pool.query(`UPDATE runrun.organizers SET organizer_name = '${name}', surname = '${surname}', email = '${email}', hash = '${password}'  WHERE id_organizer = ${id_organizer}`)
  } else {
    throw Error('ERROR: El organizador es obligatorio!');
  }
  const result = await pool.query(`SELECT * FROM runrun.organizers WHERE id_organizer = '${id_organizer}'`);
  client.release();
  return result;
}

const login = async (email) => {
  const client = await pool.connect();
  const query = `SELECT * FROM runrun.organizers WHERE email = '${email}'`;
  const result = await pool.query(query);
  if (result.rows.length === 0) {
    throw Error('Usuario incorrecto');
  }
  const user = result.rows[0];

  client.release();

  return user;
}

const saveEvent = async (name, place, date, time, id_organizer) => {
  const client = await pool.connect();
  let query = `SELECT event_name FROM runrun.events WHERE event_name = '${name}' AND event_date = '${date}' `;
  let result = await pool.query(query);
  if (result.rows.length !== 0) {
    throw Error('ERROR: Ya existe un evento con ese nombre en esa fecha!');
  }
  query = `INSERT INTO runrun.events (event_name, place, event_date, start_time, id_organizer) 
  values ('${name}', '${place}','${date}','${time}', ${id_organizer})`;
  result = await pool.query(query);
  if (result.rowCount === 0) {
    throw Error('Error de inserción!');
  }
  query = `SELECT * FROM runrun.events WHERE event_name = '${name}' AND event_date = '${date}' `;
  result = await pool.query(query);
  const event = result.rows[0];
  client.release();
  return event;
}

const removeEvent = async (idEvent) => {
  const client = await pool.connect();
  let query = `DELETE FROM runrun.participants WHERE id_event = ${idEvent}`;
  let result = await pool.query(query);

  query = `DELETE FROM runrun.events WHERE id_event = ${idEvent}`;
  result = await pool.query(query);
  client.release();
  return result;
}

const removeProfile = async (idOrganizer) => {
  const client = await pool.connect();
  let query = `DELETE FROM runrun.participants WHERE id_event IN (SELECT id_event FROM runrun.events WHERE id_organizer = ${idOrganizer})`;
  let result = await pool.query(query);
  query = `DELETE FROM runrun.events WHERE id_organizer = ${idOrganizer}`;
  result = await pool.query(query);
  query = `DELETE FROM runrun.organizers WHERE id_organizer = ${idOrganizer}`;
  result = await pool.query(query);
  client.release();
  return result;
}

const updateEvent = async (name, place, date, time, id_organizer, idEvent) => {
  if (idEvent) {
    await pool.query(`UPDATE runrun.events SET event_name = '${name}', place = '${place}', event_date = '${date}', start_time = '${time}'  WHERE id_event = '${idEvent}' AND id_organizer = '${id_organizer}'`)
  } else {
    await pool.query(`SELECT * FROM runrun.events WHERE id_event = '${idEvent}'`)
  }
  return result = await pool.query(`SELECT * FROM runrun.events WHERE id_event='${idEvent}'`)
}

const getEvents = async (name, place, date, idorganizer) => {

  if (!idorganizer)
    throw Error('ERROR: El organizador es obligatorio!');

  let query = `SELECT * FROM runrun.events WHERE id_organizer = ${idorganizer}`

  if (name && name !== '')
    query += ` AND event_name like'${name}%'`;
  if (place && place !== '')
    query += ` AND place like '${place}%'`;

  const result = await pool.query(query);

  let dateFilter;

  if (date && date !== '') {
    dateFilter = moment(date, 'DD/MM/YYYY');
  } else {
    dateFilter = moment().format('DD/MM/YYYY')
  }

  const filteredResults = result.rows.filter(row => moment(row.event_date, 'DD/MM/YYYY').isSameOrAfter(moment(dateFilter, 'DD/MM/YYYY')))

  return filteredResults;
}

const checkParticipants = async (element) => {
  const client = await pool.connect();
  let query = `SELECT * FROM runrun.participants 
    WHERE (participant_name, surname, dni, email, gender, birthdate, team, number_type, tutor_name, tutor_surname, tutor_email, id_event) = 
    ('${element.Nombre_CORREDOR}','${element.Apellidos_CORREDOR}','${element.DNI_CORREDOR}','${element.Email_CORREDOR}','${element.Sexo_CORREDOR}',
     '${element.Fecha_nacimiento_CORREDOR}','${element.Nombre_EQUIPO}','${element.Tipo_DORSAL}','${element.Nombre_TUTOR}','${element.Apellidos_TUTOR}',
     '${element.Email_TUTOR}','${element.idevent}')`;
  let result = await client.query(query);
  if (result.rows.length !== 0) {
    console.log('Usuario existente!');
  } else {
    saveParticipants(element);
  }
  client.release();
  return result;
}

const saveParticipants = async (element) => {
  const client = await pool.connect();
  console.log("Insertando participante de carrera...");
  let query = `INSERT INTO runrun.participants 
    (participant_name, surname, dni, email, gender, birthdate, team, number_type, tutor_name, tutor_surname, tutor_email, number, color, id_event)
    VALUES ('${element.Nombre_CORREDOR}','${element.Apellidos_CORREDOR}','${element.DNI_CORREDOR}','${element.Email_CORREDOR}','${element.Sexo_CORREDOR}',
            '${element.Fecha_nacimiento_CORREDOR}','${element.Nombre_EQUIPO}','${element.Tipo_DORSAL}','${element.Nombre_TUTOR}','${element.Apellidos_TUTOR}',
            '${element.Email_TUTOR}','${element.number}','${element.color}','${element.idevent}')`;
  let result = await client.query(query);
  if (result.rowCount === 0) {
    throw Error('Error de inserción!');
  } else {
    console.log("Participantes de carrera insertados!");
  }
  client.release();
  return;
}

const getParticipantsLength = async (idevent) => {
  if (!idevent)
    throw Error('ERROR: El idevent es obligatorio!');

  let query = `SELECT COUNT(id_participant) FROM runrun.participants WHERE id_event = ${idevent}`
  const result = await pool.query(query);
  return result.rows;
}

const getParticipants = async (name, surname, dni, email, team, number, color, numberType, tutorEmail, idevent) => {

  if (!idevent)
    throw Error('ERROR: El idevent es obligatorio!');

  let query = `SELECT * FROM runrun.participants WHERE id_event = ${idevent}`

  if (name && name !== '')
    query += ` AND participant_name like '${name}%'`;
  if (surname && surname !== '')
    query += ` AND surname like '${surname}%'`;
  if (dni && dni !== '')
    query += ` AND dni like '${dni}%'`;
  if (email && email !== '')
    query += ` AND email like '${email}%'`;
  if (color && color !== '')
    query += ` AND color like '${color}%'`;
  if (numberType && numberType !== '')
    query += ` AND number_type like '${numberType}%'`;
  if (team && team !== '')
    query += ` AND team like '${team}%'`;
  if (number && number !== '')
    query += ` AND number = '${number}'`;
  if (tutorEmail && tutorEmail !== '')
    query += ` AND tutor_email like '${tutorEmail}%'`;

  query+= `ORDER BY number`
  const result = await pool.query(query);
  return result.rows;
}

const getParticipant = async (id_participant) => {
  if (!id_participant)
    throw Error('ERROR: El ID de participante es obligatorio!');
  let query = `SELECT * FROM runrun.participants WHERE id_participant = ${id_participant}`
  const result = await pool.query(query);
  return result;
}

const getMailingData = async (id_participant) => {
  const client = await pool.connect();
  if (!id_participant)
    throw Error('ERROR: El ID de participante es obligatorio!');
  let checkAvailableMail = await pool.query(`SELECT email FROM runrun.participants WHERE id_participant = ${id_participant}`);
  if (checkAvailableMail.rows.length === 0) {
    participantQuery = `SELECT tutor_email, number, taken_by FROM runrun.participants WHERE id_participant = ${id_participant}`
  } else {
    participantQuery = `SELECT email, number, taken_by FROM runrun.participants WHERE id_participant = ${id_participant}`
  }
  const participantResult = await pool.query(participantQuery);
  const participantData = participantResult.rows[0];
  const participantDataToArray = Object.values(participantData);
  const takerEmail = participantDataToArray.slice(2, 3).join();
  takerQuery = `SELECT participant_name, surname FROM runrun.participants WHERE email = '${takerEmail}'`
  const takerResult = await pool.query(takerQuery);
  const takerData = takerResult.rows[0];
  const data = { ...participantData, ...takerData };
  client.release();
  return data;
}

const updateParticipant = async (delivered, taken_by, id_participant) => {
  if (delivered) {
    await pool.query(`UPDATE runrun.participants SET delivered = '${delivered}', taken_by = '${taken_by}' WHERE id_participant = '${id_participant}'`)
  } else {
    await pool.query(`UPDATE runrun.participants SET delivered = '${delivered}', taken_by = NULL WHERE id_participant = '${id_participant}'`)
  }
}

const editingParticipant = async (participant_name, surname, color, email, id_participant) => {
  await pool.query(`UPDATE runrun.participants SET participant_name = '${participant_name}', surname = '${surname}', color = '${color}', email = '${email}' WHERE id_participant = '${id_participant}'`)
}

module.exports = {
  saveUsers,
  login,
  saveEvent,
  removeEvent,
  pool,
  getEvents,
  getParticipants,
  checkParticipants,
  saveParticipants,
  updateParticipant,
  getParticipant,
  getMailingData,
  updateEvent,
  updateUsers,
  removeProfile,
  editingParticipant,
  getParticipantsLength
}