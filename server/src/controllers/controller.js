'use strict'

const fs = require('fs')
const http = require('http')
const bodyParser = require('body-parser')

const jwtToken = require('jwt-simple');
const validator = require('validator');
const bcrypt = require('bcrypt');

const multer = require('multer')
const upload = multer({ dest: "src/storage/" });
const csv = require('csvtojson')


const jwt = require('../services/jwt');
const mail = require('../services/mail');
const db = require('../db');

const validTokens = {};


// Registro de usuarios:
const register = async (req, res) => {
  const { name, surname, email, password } = req.body;
  try {
    const hashedPassword = await bcrypt.hash(password, 12);
    await db.saveUsers(name, surname, email, hashedPassword);
    const data = await _login(email, password);
    res.send(data);
  } catch (e) {
    console.log(e.message);
    res.status(500).send(e);
  }
}

// Login:
const login = async (req, res) => {
  try {
    const { email, password } = req.body;
    const data = await _login(email, password);
    res.send(data);
  } catch (e) {
    console.log(e.message);
    res.send(500).send();
  }
}

// Edición de datos de usuario:
const editUsers = async (req, res) => {
  const { name, surname, email, password, id_organizer } = req.body;
  try {
    const hashedPassword = await bcrypt.hash(password, 12);
    await db.updateUsers(name, surname, email, hashedPassword, id_organizer);
    const data = await _login(email, password);
    res.send(data);
  } catch (e) {
    console.log(e.message);
    res.status(500).send(e);
  }
}

// Crear eventos:
const createEvent = async (req, res) => {
  const { name, place, date, time, id_organizer } = req.body;
  try {
    const result = await db.saveEvent(name, place, date, time, id_organizer);
    res.send(result);
  } catch (e) {
    console.log(e.message);
    res.status(500).send(e);
  }
}

// Borrado de eventos:
const deleteEvent = async (req, res) => {
  const idEvent = req.param("id");
  try {
    const result = await db.removeEvent(idEvent);
    res.send(result);
  } catch (e) {
    console.log(e.message);
    res.status(500).send(e);
  }
}

//Borrado de organizadores
const deleteProfile = async (req, res) => {
  const idOrganizer = req.param("id");
  try {
    const result = await db.removeProfile(idOrganizer);
    res.send(result);
  } catch (e) {
    console.log(e.message);
    res.status(500).send(e);
  }
}

// Edición de eventos:
const updateEvent = async (req, res) => {
  const { name, place, date, time, id_organizer, idEvent } = req.body;
  try {
    const result = await db.updateEvent(name, place, date, time, id_organizer, idEvent);
    res.send(result);
  } catch (e) {
    console.log(e.message);
    res.status(500).send(e);
  }
}

// Búsqueda de eventos:
const getEvents = async (req, res) => {
  const name = req.param('name');
  const place = req.param('place');
  const date = req.param('date');
  const idorganizer = req.param('idorganizer');

  try {
    const events = await db.getEvents(name, place, date, idorganizer);
    res.send(events);
  } catch (e) {
    console.log(e.message);
    res.status(500).send(e)
  }
}

// Cargar participantes:
const uploadCsv = async (req, res, next) => {
  let uploaded = false;
  const file = req.file
  const idEvent = req.param('idevent');
  if (!file) {
    const error = new Error('Adjunta un fichero .csv de participantes, por favor!')
    error.httpStatusCode = 400
    return next(error)
  }
  const csvObject = await csv({ ignoreEmpty: true }).fromFile(req.file.path);
  let colors = ["rojo", "verde", "azul", "amarillo", "magenta", "cian", "negro"];
  let colorIndex = 0;
  let i = 0;
  try {
    csvObject.forEach((element) => {
      element.idevent = idEvent;
      element.number = i;
      i++;
      element.color = colors[colorIndex];
      if (element.DNI_CORREDOR === undefined) {
        element.DNI_CORREDOR = "-";
      }
      if (element.Email_CORREDOR === undefined) {
        element.Email_CORREDOR = "-";
      }
      if (element.Nombre_EQUIPO === undefined) {
        element.Nombre_EQUIPO = "-";
      }
      if (element.Email_TUTOR === undefined) {
        element.Email_TUTOR = "-";
      }
      if (i % 25 == 0) {
        colorIndex++;
      }
      db.checkParticipants(element);
    });
    fs.unlinkSync(req.file.path); // Borra archivo temporal.
    res.send(uploaded);
    console.log(csvObject.length);
    console.log('He terminado de cargar los participantes en la base de datos')
  } catch (e) {
    console.log(e.message);
    res.status(500).send(e);
  }
}

const numberOfParticipants = async (req, res) => {
  const idevent = req.param('idevent');
  try {
    const participantsLength = await db.getParticipantsLength(idevent);
    console.log(participantsLength)
    res.send(participantsLength);
  } catch (e) {
    console.log(e.message);
    res.status(500).json(e)
  }
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Asignación de tutores:
// const setTutors = async (csvObject) => {
//   csvObject.forEach((element) => {
//     console.log(element.Nombre_CORREDOR);
//     if (element.Email_TUTOR) {
//       console.log(element.Email_TUTOR);
//       db.pool.query(`SELECT id_participant FROM runrun.participants WHERE email = '${element.Email_TUTOR}'`)
//         .then((result) => {
//           db.pool.query(`UPDATE runrun.participants SET id_tutor = '${result}' WHERE email =
//                                       '${element.Email_CORREDOR}'`)
//             .then(result => console.log(result))
//             .catch(err => console.log(err));
//         });
//     }
//   });
// }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Búsqueda de participantes:
const selectParticipant = async (req, res, next) => {
  const name = req.param('participant_name');
  const surname = req.param('surname');
  const dni = req.param('dni');
  const email = req.param('email');
  const team = req.param('team');
  const number = req.param('number');
  const color = req.param('color');
  const numberType = req.param('number_type');
  const tutorEmail = req.param('tutor_email')
  const idevent = req.param('idevent');

  try {
    const participants = await db.getParticipants(name, surname, dni, email, team, number, color, numberType, tutorEmail, idevent);
    res.send(participants);
  } catch (e) {
    console.log(e.message);
    res.status(500).json(e)
  }
}

const _login = async (email, password) => {

  const user = await db.login(email);
  const samePassword = await bcrypt.compare(password, user.hash);
  if (!samePassword) {
    throw Error('Usuario incorrecto');
  }
  const payload = { id: user.id_organizer, exp: 300 };
  const secret = 'rijoiejgoiejo';
  const token = jwtToken.encode(payload, secret);
  validTokens[token];

  return { user, token };
}

// Asignación de dorsales:
const updateParticipant = async (req, res) => {
  const { id_participant, delivered, taken_by } = req.body;
  try {
    await db.updateParticipant(delivered, taken_by, id_participant);
    const result = await db.getParticipant(id_participant);
    if (result.rows.length == 0) {
      res.status(404).send('No encontrado');
    } else {
      res.send(result.rows[0]);
    }
  } catch (e) {
    console.log(e.message);
    res.status(500).send(e);
  }
  try {
    const mailingData = await db.getMailingData(id_participant);
    mail.deliveredNumberMail(mailingData);
  } catch (e) {
    console.log(e.message);
  }
}

//Editar participante:
const editParticipant = async (req, res) => {
  const { id_participant, participant_name, surname, color, email } = req.body;
  try {
    await db.editingParticipant(participant_name, surname, color, email, id_participant);
    const result = await db.getParticipant(id_participant);
    if (result.rows.length === 0) {
      res.status(404).send('No encontrado');
    } else {
      res.send(result.rows[0]);
    }
  } catch (e) {
    console.log(e.message)
    res.status(500).send(e);
  }
}

module.exports = {
  register,
  login,
  createEvent,
  deleteEvent,
  uploadCsv,
  selectParticipant,
  getEvents,
  updateParticipant,
  updateEvent,
  editUsers,
  deleteProfile,
  editParticipant,
  numberOfParticipants
};