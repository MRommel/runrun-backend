create database runrundb;

-- DROP SCHEMA runrun;

CREATE SCHEMA runrun AUTHORIZATION postgres;

-- DROP TYPE runrun."_events";

CREATE TYPE runrun."_events" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = runrun.events,
	DELIMITER = ',');

-- DROP TYPE runrun."_organizers";

CREATE TYPE runrun."_organizers" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = runrun.organizers,
	DELIMITER = ',');

-- DROP TYPE runrun."_participants";

CREATE TYPE runrun."_participants" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = runrun.participants,
	DELIMITER = ',');

-- DROP SEQUENCE runrun.events_id_event_seq;

CREATE SEQUENCE runrun.events_id_event_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START 37
	CACHE 1
	NO CYCLE;

-- DROP SEQUENCE runrun.organizers_id_organizer_seq;

CREATE SEQUENCE runrun.organizers_id_organizer_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START 13
	CACHE 1
	NO CYCLE;

-- DROP SEQUENCE runrun.participants_id_participant_seq;

CREATE SEQUENCE runrun.participants_id_participant_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START 705
	CACHE 1
	NO CYCLE;
	
-- Drop table

-- DROP TABLE runrun.organizers;

CREATE TABLE runrun.organizers (
	id_organizer serial NOT NULL,
	organizer_name varchar(100) NOT NULL,
	surname varchar(100) NOT NULL,
	email text NOT NULL,
	hash text NOT NULL,
	CONSTRAINT pk_organizers PRIMARY KEY (id_organizer)
);

-- Drop table

-- DROP TABLE runrun.events;

CREATE TABLE runrun.events (
	id_event serial NOT NULL,
	event_name text NOT NULL,
	place text NOT NULL,
	event_date text NOT NULL,
	start_time text NOT NULL,
	id_organizer int4 NULL,
	CONSTRAINT pk_events PRIMARY KEY (id_event),
	CONSTRAINT events_fk FOREIGN KEY (id_organizer) REFERENCES runrun.organizers(id_organizer)
);

-- Drop table

-- DROP TABLE runrun.participants;

CREATE TABLE runrun.participants (
	id_participant serial NOT NULL,
	participant_name varchar(100) NOT NULL,
	surname varchar(255) NOT NULL,
	dni varchar(10) NULL,
	"number" int4 NULL,
	email varchar(255) NULL,
	gender varchar(15) NULL,
	birthdate date NULL,
	id_tutor int4 NULL,
	relationship varchar(20) NULL,
	team varchar(100) NULL,
	taken_by varchar(255) NULL,
	number_type varchar(20) NULL,
	color varchar(20) NULL,
	id_event int4 NULL,
	tutor_name varchar(100) NULL,
	tutor_surname varchar(100) NULL,
	tutor_email varchar(100) NULL,
	delivered bool NULL,
	CONSTRAINT pk_participants PRIMARY KEY (id_participant),
	CONSTRAINT participants_fk FOREIGN KEY (id_event) REFERENCES runrun.events(id_event)
);
